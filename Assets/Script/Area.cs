﻿using UnityEngine;
using UnityEditor;
using Valve.VR.InteractionSystem;
using UnityEngine.Events;

public class Area : MonoBehaviour
{
    public LevelCore core;
    public GameObject pref;
    public float[] size = new float[4];
    public Color32 color;
    public Vector3 sizeGizmo;
    Random ran;
    Gizmos gizmos;
    public bool isCreateted;

    private void Start()
    {
        ran = new Random();
        Vector2 size1;

        size1.x = sizeGizmo.x / 2;
        size1.y = sizeGizmo.z / 2;
        size[0] = transform.position.x - size1.x;
        size[1] = transform.position.x + size1.x;
        size[2] = transform.position.z - size1.y;
        size[3] = transform.position.z + size1.y;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawWireCube(transform.position, sizeGizmo);
    }

    public void CreateSphire()
    {
        Vector3 v;

        v.x = Random.Range(size[0], size[1]);
        v.y = 0.5f;
        v.z = Random.Range(size[2], size[3]);
        GameObject g = GameObject.Instantiate(pref);
        g.AddComponent<c>().area = this;
        if (g.GetComponent<Throwable>())
            g.GetComponent<Throwable>().onPickUp.AddListener(new UnityAction(core.Randobj));
        g.transform.position = v;
    }
}