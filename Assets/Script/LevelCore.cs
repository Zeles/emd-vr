﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelCore : MonoBehaviour
{
    [Header("MaxMin")]
    public int MaxAreaID;

    public Area thisarea;
    public List<Area> area;

    System.Random ran;
    private int randarea;

    public void Start()
    {
        ran = new System.Random();
        Randobj();
    }

    public void Randobj()
    {
        bool b = false;
        for (int i = 0; i < area.Count; i++)
        {
            if (!area[i].isCreateted)
                b = true;
        }
        if (b)
        {
            R();
            thisarea = area[randarea];
            thisarea.CreateSphire();
            thisarea.isCreateted = true;
        }
    }

    private void R()
    {
        randarea = ran.Next(0, MaxAreaID);
        if (area[randarea].isCreateted)
            R();
    }
}