﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
class CreateScene
{
    static CreateScene()
    {
        EditorSceneManager.newSceneCreated += EditorSceneManager_newSceneCreated;
    }

    static void EditorSceneManager_newSceneCreated(UnityEngine.SceneManagement.Scene scene, NewSceneSetup setup, NewSceneMode mode)
    {
        GameObject light = GameObject.Find("Directional Light");
        GameObject cam = Camera.main.gameObject;
        Transform setup1 = new GameObject("SETUP").transform;
        cam.transform.SetParent(setup1);
        light.transform.SetParent(setup1);

        Transform world = new GameObject("WORLD").transform;
        GameObject din = new GameObject("Dinamic");
        din.transform.SetParent(world);
        GameObject stat = new GameObject("Static");
        stat.transform.SetParent(world);

        Transform ui = new GameObject("UI").transform;
    }
}
