﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelCore))]
public class LevelCoreEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Gen"))
            Fi();
    }

    void Fi()
    {
        LevelCore tar = (LevelCore)target;
        tar.area = new List<Area>();
        if (tar != null)
        {
            GameObject[] allarea = GameObject.FindGameObjectsWithTag("Area");
            for (int i = 0; i < allarea.Length; i++)
            {
                if (allarea[i].GetComponent<Area>())
                {
                    tar.area.Add(allarea[i].GetComponent<Area>());
                    allarea[i].GetComponent<Area>().core = tar;
                }
            }
            tar.MaxAreaID = tar.area.Count;
        }
    }
}
